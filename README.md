# RFID auth with arduino for P30 KeContact #

Code for Arduino board with RDM6300 RFID reader to add token authorisation at P30 KeContact charging station.

### RDM6300 RFID reader ###

* Manage add/remove token with master card token
* version 0.6
* [YouTube video](https://youtu.be/C1fEi6Gjn2Y)

### Needed HW ###

* RDM6300 reader
* Arduino (nano)
* [rdm6300 library](https://github.com/arduino12/rdm6300)
* At least 2 RFID tags (125kHz)
* KeContact P30 charging station

### Workflow ###

```
                                    +---------+
  +----------------------------------->READ TAGS+^------------------------------------------+
  |                              +--------------------+                                     |
  |                              |                    |                                     |
  |                              |                    |                                     |
  |                         +----v-----+        +-----v----+                                |
  |                         |MASTER TAG|        |OTHER TAGS|                                |
  |                         +--+-------+        ++-------------+                            |
  |                            |                 |             |                            |
  |                            |                 |             |                            |
  |                      +-----v---+        +----v----+   +----v------+                     |
  |         +------------+READ TAGS+---+    |KNOWN TAG|   |UNKNOWN TAG|                     |
  |         |            +-+-------+   |    +-----------+ +------------------+              |
  |         |              |           |                |                    |              |
  |    +----v-----+   +----v----+   +--v--------+     +-v--------------+  +------v----+     |
  |    |MASTER TAG|   |KNOWN TAG|   |UNKNOWN TAG|     |SWITCH          |  | NOT AUTH  |     |
  |    +-----+----+   +---+-----+   +-----+-----+     |CHARGING STATION|  | SOUND     |     |
  |          |            |               |           |LOCK / UNLOCK   |  +-----+-----+     |
  |       +--v-+     +----v------+     +--v---+       +-----+----------+        |           |
  +-------+EXIT|     |DELETE FROM|     |ADD TO|             |                   +-----------+
          +----+     |  EEPROM   |     |EEPROM|             +-------------------------------+
                     +-----------+     +------+
```

### Pin layout ###

```
   ---------------------------------------------
                          Arduino       Arduino
                          Uno/101       Nano v3
   Signal                 Pin           Pin       
   ---------------------------------------------
   SerialRX RDM6300 TX    3             D3
   SerialTX RDM6300 RX    2             D2       (optional)
   Relay signal           4             D4
   Piezo/buzzer           8             D8
   
   RST/Reset              9             D9
   Blue LED               5             D5
   Green LED              6             D6
   Red LED                7             D7
```

### P30 KeContact X1 ###

Enable input `X1` : `DWS 1.1` `on` 

```
              ___
  open:  ---o/   o-- not authorized to start charging
  close: ---o/---o-- authorized to start charging
```

### Contact ###

* https://www.jelmoni.com/kcp30-rfid-auth
